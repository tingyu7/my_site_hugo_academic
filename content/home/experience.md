+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
subtitle = ""

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Firmware Engineer"
  company = "Gogoro"
  company_url = "https://www.gogoro.com/"
  location = "Taoyuan, Taiwan"
  date_start = "2018-09-12"
  date_end = ""
  description = """
  Design and develop embedded software and firmware for BMS, charging station and third party tool
  """

[[experience]]
  title = "Electrical Engineering Intern"
  company = "Gogoro"
  company_url = "https://www.gogoro.com/"
  location = "Taoyuan, Taiwan"
  date_start = "2017-06-28"
  date_end = "2017-08-28"
  description = """
  Analyze field returned battery packs to determine firmware and hardware bugs
  """

[[experience]]
  title = "Product Validation Engineering Intern"
  company = "Daimle Trucks North America"
  company_url = "https://daimler-trucksnorthamerica.com/"
  location = "Portland, Oregon"
  date_start = "2016-07-11"
  date_end = "2016-12-23"
  description = """
  Perform software simulation and data analysis in validating performance of Daimler trucks
  """

+++
