---
# Display name
title: Jacky Wang

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Embedded Software and Firmware Engineer

# Organizations/Affiliations
organizations:
- name: Firmware Engineer @ Gogoro
  url: "https://www.gogoro.com/"

# Short bio (displayed in user profile at end of posts)
bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests:
- Embedded Systems
- Energy Systems
- Remote Sensing
- Driver Assistance Systems

education:
  courses:
  - course: BSc in Electrical Engineering
    institution: University of Washington
    year: 2017

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/jacky-wang
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/tingyu7
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".  
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "jtingyu7@gmail.com"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

I enjoy working on projects where I can utilize and expand my embedded systems knowledge. 
I do not want to limit myself in working for a particular application since that is no fun.
Rather, while I still can, I want to experience the almightiness of being an embedded 
engineer by working on projects for various applications.

One of my top to-do items is to build systems for harsh environemnts such as in space.
